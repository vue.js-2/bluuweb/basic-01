const app = new Vue({
  el: "#app",
  data: {
    titulo: "Hola mundo con vue",
    frutas: [
      { nombre: "Pera", cantidad: 10 },
      { nombre: "Manzana", cantidad: 4 },
      { nombre: "Plátano", cantidad: 0 },
    ],
    nuevaFruta: "",
    nuevaCantidad: null,
    total: 0,
  },
  methods: {
    agregarFruta() {
      this.frutas.push({
        nombre: this.nuevaFruta,
        cantidad: this.nuevaCantidad,
      });
      this.nuevaFruta = "";
      this.nuevaCantidad = "";
    },
    aumentarFruta(index) {
      this.frutas[index].cantidad++;
    },
    disminuirFruta(index) {
      if (this.frutas[index].cantidad > 0) {
        this.frutas[index].cantidad--;
      }
    },
  },
  computed: {
    sumarFrutas() {
      this.total = 0;
      for (fruta of this.frutas) {
        this.total += fruta.cantidad;
      }
      return this.total;
    },
  },
});
